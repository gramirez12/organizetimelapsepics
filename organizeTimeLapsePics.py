########################################################################################################################
#   organizeTimeLapsePics.py
#
#   Python program to re-organize pictures into a new folder structure. The original pictures can be in any folder but
#   there should in the path the name of the site to make sure the pictures new archive structure is correct.
#   The site name must agree with the index of the directory.
#   This code was designed to run on Microsoft Windows OS but it can work on any OS where Python, openCV, and Tesseract.
#   e.g.:
#       path:  E:\NWERN_camera_photos\San_Luis_Valley\2018\NWERN_SLV_timelapse_20180927\
#       index: 0          1                  2          3       4
#       for the above example, the site name is San_Luis_Valley and the 'indexSiteName' = 2
#   If you want a different time stamp format in the new file name, you can change the variable timestampFormatStore
#   following the format here: https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes
#   pathStorage variable is where the new file structure will be located.
#   pathSource variable is where the pictures to reorganize is located
#   NOTE: Please note that the paths must start with an "r" before the "'" and use slash (/) instead \. Maybe you can
#   use \ if you put r before the string but I haven't tested very well. Also, instead to uses slash, you can uses
#   "\\" double diagonal.
#   pathLogFiles variable is the path where will be located the logs files that include logs of: errors, missed files,
#   temperatures, resolution files, etc.
#
#   In order to run this code, you need to have installed the following
#   INSTALLATION:
#       download and install Python 3+ from https://www.python.org/downloads/ and PIP for easy installation of libraries
#       install OpenCV from https://pypi.org/project/opencv-python/
#           pip install opencv-python
#       install Python Imaging Library (PIL) from https://pypi.org/project/Pillow/
#           pip install Pillow
#       download and install Tesseract from https://github.com/UB-Mannheim/tesseract/wiki
#           make sure you add Tesseract into the system PATH.
#           For more information go to https://tesseract-ocr.github.io/tessdoc/Home.html and
#           https://github.com/tesseract-ocr/tesseract
#       install pytesseract from https://pypi.org/project/pytesseract/
#           pip install pytesseract
#
#
#   System Ecology Lab
#   Author: Gesuri Ramirez
#   Email: gramirez12@utep.edu
#   Date: May 2020
########################################################################################################################


import cv2
import pytesseract
import datetime
import shutil
import zipfile
import tempfile
from PIL import Image
from pathlib import Path


pathSource = r'E:/NWERN_camera_photos'
pathStorage = r'E:/sites'
timestampFormatStorage = '%Y%m%d_%H%M%S'
pathLogFiles = r'E:/meta'

# path:  E:\NWERN_camera_photos\San_Luis_Valley\2018\NWERN_SLV_timelapse_20180927\
# index: 0          1                  2          3       4
# for the above example, the site name is San_Luis_Valley and the index is 2
indexSiteName = 2



dirStorage = Path(pathStorage)
dirP = pathSource
logFiles = Path(pathLogFiles)

def log(line):
    """ Log a message into a file called 'logs.txt' locaded in logFiles"""
    l = logFiles.joinpath('logs.txt')
    print(line)
    with l.open('a+') as f:
        f.write(f'{line}\n')


def logMissedFile(fi, err=None):
    """ Log a missed file into '<logFiles>/logMissedFiles.txt
    It can happened when the original file is corrupted'"""
    lf = logFiles.joinpath('logMissedFiles.txt')
    line = f'<FileError> {str(fi)}, error: {str(err)}'
    print(line)
    with lf.open('a+') as f:
        f.write(f'{line}\n')


def showImg(img, title = 'tt'):
    """ Shows the image using openCV. Not really needed this function. """
    cv2.imshow(title, img)
    cv2.waitKey(0)


def get_date_taken(path, timestampFormat='%Y:%m:%d %H:%M:%S'):
    """ Get the date of the exif of the jpeg picture. Return a datetime object if the date in the exif data
    path: the path of the picture
    timestampFormat: the format to recognize the date and conver into a datetime object """
    dt = Image.open(path)._getexif()[36867]
    return datetime.datetime.strptime(dt, timestampFormat)


def getSiteData(imagePath, timestampFormat = '%Y-%m-%d %I:%M:%S %p'):
    """ Tries to read the text of the picture and by using optical character recognition (OCR)
    if the date of the picture is different to the date in the exif, the exif date is used
    This function return the date in a datetime object, text temperature in C, the site name (from OCR), and a
        dictionary with the picture X ('rX') and Y ('rY') size
    imagePath: the image path
    timestampFormat: the format to recognize the date and convert into a datetime object """
    moreInfo = {}
    # get timestamp from image
    timestampDT = get_date_taken(imagePath)
    # read image
    img = cv2.imread(imagePath, cv2.IMREAD_GRAYSCALE)
    moreInfo['rX'] = img.shape[1]
    moreInfo['rY'] = img.shape[0]
    # erase image top line not needed needed
    img = cv2.rectangle(img, (770, 0), (img.shape[1]-190, 31), (0, 0, 0), -1)
    # clearing next lines
    img = cv2.rectangle(img, (0, 31), (img.shape[1], 200), (0, 0, 0), -1)
    # moving lower line to second line
    img[45:45 + 31, 0:1000] = img[img.shape[0] - 31:img.shape[0], 0:1000]
    # erasing not needed image
    img = cv2.rectangle(img, (0, 45 + 32), (img.shape[1], img.shape[0]), (0, 0, 0), -1)
    # cropping image
    img = img[0:100, 0:img.shape[1]]
    # converting to binary image
    img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    # inverting colors
    img = cv2.bitwise_not(img)
    # removing noise
    # img = cv2.medianBlur(img, 3)
    # read text in image
    text = pytesseract.image_to_string(img, config=r'--oem 3 --psm 6 --dpi 72')
    #print(text)
    text = text.splitlines()
    pos = text[0].rfind('M ')
    dt = text[0][:pos+1]
    try:
        dt = datetime.datetime.strptime(dt, timestampFormat)
    except ValueError:
        dt = None
    temp = text[0][pos+1:]
    try:
        if '°C' in temp:
            temp = float(temp.replace('°C', ''))
            temp = "{:.2f}".format(temp)
        elif '°F' in temp:
            temp = (float(temp.replace('°F', '')) - 32) / 1.8
            temp = "{:.2f}".format(temp)
    except ValueError:
        pass
    #print(temp)
    siteName = text[-1]
    siteName = siteName.replace(' ', '_')
    #print(siteName)
    if dt != timestampDT:
        log('<ERROR> Not same timestamp from image and metadata. Returning timestamp from metadata.')
        dt = timestampDT
    return dt, temp, siteName, moreInfo


def tree(directory):
    """ print the whole directory. Not really needed this function. """
    log(f'+ {directory}')
    for path in sorted(directory.rglob('*')):
        depth = len(path.relative_to(directory).parts)
        spacer = '    ' * depth
        log(f'{spacer}+ {path.name}')


def organizeData(dirPath, originalPath=None):
    """ Each picture or zip file with pictures will be processed in order to copy into the new structure
    This function look for the pictures and if there is a zip file, it is unzipped and search for pictures
    If the picture of zip file is corrupted, it is logged in missed files log. Also it is logged the picture X&Y size,
    temperature, site name from OCR.
    The site name will be read from the path of the picture or zip file.
    This functions ir recursive."""
    if 'DONE!!!' in str(dirPath) or 'DONE!!!' in str(originalPath):
        log(f'This Folder is already completed: {str(dirPath)} | {str(originalPath)}')
        return
    if not logFiles.exists(): # check and create logFiles folder
        logFiles.mkdir(parents=True, exist_ok=True)
    for item in dirPath.iterdir():
        if item.is_file(): # if file do something
            #log(f'Working in {item}')
            if item.suffix.lower() == '.jpg' or item.suffix.lower() == '.jpeg': # if image get data from image and copy to storage
                itemDT, itemTemp, itemSite, mi = getSiteData(str(item)) # get data from image ans exif
                if not originalPath:
                    realSite = item.parts[indexSiteName] # get the site name
                else:
                    realSite = originalPath.parts[indexSiteName]
                # create the new name of the image
                newFileName = realSite + '_' + itemDT.strftime(timestampFormatStorage) + '.jpg'
                # create the new storage path
                newItemPath = dirStorage.joinpath(realSite).joinpath(str(itemDT.year)).joinpath(f'{itemDT.month:02}').joinpath(newFileName)
                # if the read site is != to the real site, it is log
                if realSite != itemSite:
                    logSiteName = logFiles.joinpath(itemSite + '.txt')
                    with logSiteName.open('a+') as f:
                        f.write(f'{str(newItemPath)}\n')
                # log the resolution of the image
                resFile = logFiles.joinpath(f'{mi["rX"]}x{mi["rY"]}.txt')
                if not newItemPath.parent.exists():
                    newItemPath.parent.mkdir(parents=True, exist_ok=True)
                with resFile.open('a+') as f:
                    f.write(f'{str(newItemPath)}\n')
                # log the read temperature per site
                tFile = logFiles.joinpath(f'temp_{realSite}_{itemDT.year}.csv')
                with tFile.open('a+') as f:
                    f.write(f'{newItemPath.name},{itemTemp}\n')
                log(f'{item} >> {newFileName}')
                # copy and rename the image to the storage site with the new format (siteName_YYYYMMdd_HHmmss)
                try:
                    shutil.copy2(str(item), newItemPath)
                except IOError as e:
                    logMissedFile(f'IOError: {str(item)} >> {str(newItemPath)}', e)
                except PermissionError as e:
                    logMissedFile(f'PermissionError: {str(item)} >> {str(newItemPath)}', e)
                except:
                    logMissedFile(f'Unknow: {str(item)} >> {str(newItemPath)}', e)
            # if the item is a file but it is a zip, it will be unzipped and call this function again
            elif item.suffix.lower() == '.zip':
                log(f'ZIP file: {item}')
                tempDir = tempfile.mkdtemp() # create a new temporal folder
                # unzip all the files into the temporal directory
                try:
                    with zipfile.ZipFile(str(item), 'r') as zip_ref:
                        zip_ref.extractall(tempDir)
                except zipfile.BadZipFile as e:
                    logMissedFile(f'Bad zip file: {item}', e)
                    return
                # call this function again, recursion!
                organizeData(Path(tempDir), item)
        # if there is a folder the item call this function again, recursion!
        elif item.is_dir():
            organizeData(item, originalPath)
        # if there is another thing, just skip it
        else:
            return


if __name__ == '__main__':
    pth = Path(dirP)
    organizeData(pth)


