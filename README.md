# organizeTimeLapsePics.py
Python program to re-organize pictures into a new folder structure. 
 
### Usage
The original pictures can be in any folder but there should in the path the name of the site to make sure the pictures new archive structure is correct.
This code was designed to run on Microsoft Windows OS but it can work on any OS where Python, openCV, and Tesseract are working.
To run the code can be in command line by using: python organizeTimeLapsePics.py
To run correctly you must to have installed Python 3+, openCV, and Tesseract. Also, you need to go into the code and change the  variable paths as decrived below. 
 
The site name must agree with the index of the directory.
  
e.g.:
 
 ``` 
path:  E:\NWERN_camera_photos\San_Luis_Valley\2018\NWERN_SLV_timelapse_20180927\ 
index: 0  \       1          \       2       \  3 \      4 
for the above example, the site name is San_Luis_Valley and the 'indexSiteName' = 2
 ```

If you want a different time stamp format in the new file name, you can change the variable timestampFormatStore
following the format here: [Python format codes](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes)
   
###### pathStorage
is where the new file structure will be located.
###### pathSource
variable is where the pictures to reorganize is located
### NOTE: 
Please note that the paths must start with an "r" before the "'" and use slash (/) instead \. Maybe you can
use \ if you put r before the string but I haven't tested very well. Also, instead to uses slash, you can uses
"\\" double diagonal.
###### pathLogFiles
variable is the path where will be located the logs files that include logs of: errors, missed files,
temperatures, resolution files, etc.

## INSTALLATION:
In order to run this code, you need to have installed the following
* download and install [Python 3+](https://www.python.org/downloads/) and PIP for easy installation of libraries
* install [OpenCV](https://pypi.org/project/opencv-python/)
```
pip install opencv-python
```
* install Python Imaging Library (PIL) from https://pypi.org/project/Pillow/
```
pip install Pillow
```
* download and install [Tesseract](https://github.com/UB-Mannheim/tesseract/wiki)
>    make sure you add Tesseract into the system PATH.
     For more information go to https://tesseract-ocr.github.io/tessdoc/Home.html and https://github.com/tesseract-ocr/tesseract
* install pytesseract from https://pypi.org/project/pytesseract/
```
pip install pytesseract
```

### System Ecology Lab
**Author:** Gesuri Ramirez

**Email:** gramirez12@utep.edu

**Date:** May 2020
